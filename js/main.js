let idEdit = null;

// function show dữ liệu lên web
let renderFoodList = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contentTr = `<tr>
                        <td>${monAn.id}</td>
                        <td>${monAn.name}</td>
                        <td>${monAn.price}</td>
                        <td>${monAn.description}</td>
                        <td>
                          <button class="btn btn-warning" onclick="dataToForm(${monAn.id})">Edit</button>
                          <button class="btn btn-danger" onclick="delFood(${monAn.id})">Delete</button>
                        </td>
                    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbody_food").innerHTML = contentHTML;
};

import { foodService } from "./service/foodService.js";
import { spinner } from "./service/spinner.js";
import { controller } from "./service/controller.js";
spinner.onLoading();

// function render
let renderFoodService = () => {
  foodService
    .getFoodList()
    .then((res) => {
      // console.log(res.data);
      renderFoodList(res.data);
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
renderFoodService();

// function delete item
let delFood = (idFood) => {
  spinner.onLoading();
  foodService
    .deleteFood(idFood)
    .then((res) => {
      renderFoodService();
      controller.showData({
        name:"",
        price:"",
        description:"",
      });
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
window.delFood = delFood;

// function edit item
let dataToForm = (id) => {
spinner.onLoading();
  idEdit = id;
  foodService
    .getDataEdit(id)
    .then((res) => {
      controller.showData(res.data)
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
window.dataToForm = dataToForm;
// function update item
let updateFood =()=>{
  spinner.onLoading();
  let monAn = dataFormInput();
  let newFood = {...monAn,id:idEdit}
  foodService.putFood(newFood).then((res)=>{
    renderFoodService();
    // reset form input 
    controller.showData({
      name:"",
      price:"",
      description:"",
    });
    spinner.offLoading();
  }).catch((err)=>{
    spinner.offLoading();
  });
}
window.updateFood =updateFood;

let dataFormInput = () => {
  let foodName = document.getElementById("foodName").value;
  let foodPrice = document.getElementById("foodPrice").value;
  let foodDescription = document.getElementById("foodDescription").value;

  let monAn = {
    name: foodName,
    price: foodPrice,
    description: foodDescription,
  };
  return monAn;
};

// function post item
let postFood = () => {
  spinner.onLoading();
  foodService
    .addFood(dataFormInput())
    .then((res) => {
      renderFoodService();
      controller.showData({
        name:"",
        price:"",
        description:"",
      });
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
window.postFood = postFood;
