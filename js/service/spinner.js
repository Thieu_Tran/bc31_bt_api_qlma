export const spinner ={
    onLoading:()=>{
        document.getElementById("loading").style.display ="flex";
        document.getElementById("tableFoodList").style.display ="none";
    },
    offLoading:()=>{
        document.getElementById("loading").style.display ="none";
        document.getElementById("tableFoodList").style.display ="block";
    },
}