let URL_Base = "https://62b9758941bf319d227cec4b.mockapi.io/";
export let foodService = {
  // Lấy dữ liệu
  getFoodList: () => {
    return axios({
      url: URL_Base + "mon-an",
      method: "GET",
    });
  },

  // Xóa dữ liệu
  deleteFood: (id) => {
    return axios({
      url: `${URL_Base}mon-an/${id}`,
      method: "DELETE",
    });
  },

  // thêm dữ liệu
  addFood: (monAn) => {
    return axios({
      url: URL_Base + "mon-an",
      method: "POST",
      data: monAn,
    });
  },

  getDataEdit: (id) => {
    return axios({
      url: `${URL_Base}mon-an/${id}`,
      method: "GET",
    });
  },
  // Cập nhật dữ liệu
  putFood: (monAn) => {
    return axios({
      url: `${URL_Base}mon-an/${monAn.id}`,
      method: "PUT",
      data:monAn,
    });
  },
};

